import { useSelector } from 'react-redux'

import { NoDataLottie } from 'shared/animation/lottie-gif/NoDataLottie'
import StandardCardSkeleton from 'shared/ui/cards/StandardCards/ui/skeleton'

import { TripCard } from 'entities/cards/TripCard'
import { useTripData } from 'entities/cards/TripCard/api'
import { ITripCard } from 'entities/cards/TripCard/model'

import { FavoriteButton } from 'features/buttons/FavoriteButton'
import { searchInputValueSelector } from 'features/inputs/SearchInput/model/selectors'

interface TripCardsListProps {
  searchEnabled?: boolean
  applyFilter?: (cards: ITripCard[]) => ITripCard[]
}

const TripCardsList = ({ searchEnabled = false, applyFilter }: TripCardsListProps) => {
  const searchValue = useSelector(searchEnabled ? searchInputValueSelector : () => '')
  const { data, isLoading, isError } = useTripData({ search: searchValue })

  if (isLoading) {
    return (
      <section
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          justifyContent: searchEnabled ? 'flex-start' : 'center',
          alignItems: 'flex-start',
          gap: '16px',
          maxHeight: '55vh',
          overflowY: 'auto',
        }}
      >
        {Array.from({ length: 6 }).map((_, index) => (
          <StandardCardSkeleton key={index} typeSizeCard="flexGrow" />
        ))}
      </section>
    )
  }

  if (isError) {
    return <div>ErrorPage</div>
  }

  const tripCardsList = data?.data.tripCards || []
  const filteredTripCardsList = applyFilter ? applyFilter(tripCardsList) : tripCardsList // Применяем фильтрацию к массиву карточек(опционально)

  return (
    <>
      {filteredTripCardsList.length > 0 ? (
        <section
          style={{
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: searchEnabled ? 'flex-start' : 'center',
            alignItems: 'flex-start',
            gap: '25px',
            maxHeight: '85vh',
            overflowY: 'auto',
            maxWidth: '100%',
          }}
        >
          {filteredTripCardsList.map((tripCard: ITripCard) => (
            <TripCard
              key={tripCard.id}
              {...tripCard}
              favoriteButton={
                <FavoriteButton
                  isFavorite={tripCard.favorite}
                  favoriteRequestPath={`tripCards/${tripCard.id}/favorite`}
                  tripCardId={tripCard.id}
                />
              }
            />
          ))}
        </section>
      ) : (
        <NoDataLottie title="Trip is not found" />
      )}
    </>
  )
}

export default TripCardsList
