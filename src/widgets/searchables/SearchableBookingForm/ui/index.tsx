import { useLocation } from 'react-router-dom'

import { SearchableTripsForm } from '../searchable-trips-form/ui'

const SearchableBookingForm = () => {
  const location = useLocation()

  if (location.pathname === '/hotel_searchableForm') {
    return <>hotel search</>
  }

  if (location.pathname === '/trips_searchableForm') {
    return <SearchableTripsForm />
  }
  if (location.pathname === '/transport_searchableForm') {
    return <>transport search</>
  }
  if (location.pathname === '/events_searchableForm') {
    return <>events search</>
  }
  return null
}

export default SearchableBookingForm
