import { Form, Formik } from 'formik'
import { Layout } from 'shared/layouts/Layout'
import { BackButtonWithTitle } from 'features/titles/BackButtonWithTitle'
import { SearchableSelect } from 'features/selectors/searchable-select'
import { MainButton } from 'shared/ui/buttons/MainButton'
import { SelectorButtons } from 'shared/ui/buttons/SelectorButtons'
import * as Yup from 'yup'

export type TFormikStateSearchableBookingFormRecord = Record<'region' | 'location' | 'departure' | 'arrival' | 'passenger' | 'luggage' | 'selectorType', string | number>

const formikInitialStateSearchableBookingForm: TFormikStateSearchableBookingFormRecord = {
  region: '',
  location: '',
  departure: '',
  arrival: '',
  passenger: 0,
  luggage: 0,
  selectorType: ''
}

const validationSchema = Yup.object().shape({
  region: Yup.string().required('Required'),
  location: Yup.string().required('Required'),
  departure: Yup.string().required('Required'),
  arrival: Yup.string().required('Required'),
  passenger: Yup.number()
    .required('Required')
    .moreThan(0, 'Must be greater than 0'),
  selectorType: Yup.string().required('Required'),
});

const selectorButtonsList = [
  {
    id:1,
    title:'Holiday'
  },
  {
    id:2,
    title:'Weekends'
  },
  {
    id:3,
    title:'Honeymoon trip'
  },
  {
    id:4,
    title:'HoneymoonTrip'
  },
  {
    id:5,
    title:'Honeymoonn trip'
  }
]

export const SearchableTripsForm = () => {
  

  return (
    <Layout>
      <BackButtonWithTitle title={'Trip Booking'} />
      <Formik validateOnMount initialValues={formikInitialStateSearchableBookingForm} validationSchema={validationSchema} onSubmit={(values) => { 

        console.log(values,'values')
      }}>
        {({ submitForm, isSubmitting, setFieldValue, values, isValid }) => {
          return (
            <Form style={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}>
              <SearchableSelect type='region' setFieldValue={setFieldValue} values={values} />
              <SearchableSelect type='location' setFieldValue={setFieldValue} values={values} />
              <SearchableSelect type='flightDate' setFieldValue={setFieldValue} values={values} />
              <SearchableSelect type='passenger' setFieldValue={setFieldValue} values={values} />

              <SelectorButtons buttonsList={selectorButtonsList} title='Type' onClick={(field, value) => setFieldValue(field, value)} />

              <MainButton
                type={'button'}
                variation={'large'}
                styles={{
                  backgroundColor: !isValid ? 'var(--secondary-grey)' : 'var(--primary-orange)',
                  color: 'var(--primary-white)',
                  marginTop: '1rem',
                  marginBottom: '3rem',
                  transition:'0.1s ease-in-out',
                  opacity: !isValid ? 0.3 : 1
                }}
                onClick={submitForm}
                disabled={!isValid}
              >
                {isSubmitting ? 'Loading...' : 'Search'}
              </MainButton>
            </Form>
          )
        }}
      </Formik>
    </Layout>
  )
}
