interface IWelcomeBaseData {
  id: number
  title: string
}

export interface IWelcomeDataWithImage extends IWelcomeBaseData {
  img: string
}

export interface IWelcomeSignUpData extends IWelcomeBaseData {
  subtitle: string
}
