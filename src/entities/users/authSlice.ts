// features/authSlice.js
import { createSlice } from '@reduxjs/toolkit'
import Cookies from 'js-cookie'

const initialState = {
  user: JSON.parse(Cookies.get('user') || 'null'),
  token: Cookies.get('token') || null,
}

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login: (_, { payload }) => {
      Cookies.set('token', payload.token, { expires: 7 })
      Cookies.set('user', JSON.stringify(payload.user), { expires: 7 })
      return {
        token: payload.token,
        user: payload.user,
      }
    },
    logout: state => {
      state.user = null
      state.token = null
      Cookies.remove('token')
      Cookies.remove('user')
    },
  },
})

export const { login, logout } = authSlice.actions

export default authSlice.reducer
