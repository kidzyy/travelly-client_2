import StandardCard from 'shared/ui/cards/StandardCards/ui'
import { RatingBlock } from 'shared/ui/ratingBlock'

import { ITripCard } from '../model'

interface ITripCardProps extends ITripCard {
  favoriteButton: React.ReactNode
}

export const TripCard = ({
  tripName,
  rating,
  isHotOffer,
  price,
  country,
  imageUrl,
  favoriteButton,
}: ITripCardProps) => {
  return (
    <StandardCard
      typeSizeCard="flexGrow"
      backgroundImage={imageUrl}
      favoriteButton={favoriteButton}
      ratingBlock={
        <RatingBlock
          variation="extra_sm_card"
          styles={{
            backgroundColor: 'var(--primary-white)',
            borderRadius: '7px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          rating={rating}
        />
      }
      tripName={tripName}
      country={country}
      price={price}
      isHotOffer={isHotOffer}
    />
  )
}
