import { useState } from 'react'

import { ITripCard } from '../model'

type TripDataFilterSelectors = 'all' | 'recommend' | 'popular' | 'hotOffers' | 'rating'

interface IUseFilterTripData {
  selectedFilter: TripDataFilterSelectors
  setSelectedFilter: React.Dispatch<React.SetStateAction<TripDataFilterSelectors>>
  filterTripDataOptions: TripDataFilterSelectors[]
  applyFilter: (filter: TripDataFilterSelectors, cards: ITripCard[]) => ITripCard[]
}

/**
 * Кастомный хук для фильтрации данных карточек поездок.
 *
 * @returns {IUseFilterTripData} Объект, содержащий:
 * - `selectedFilter`: Текущий выбранный тип фильтра.
 * - `setSelectedFilter`: Функция для обновления выбранного фильтра.
 * - `filterTripDataOptions`: Массив возможных опций фильтров.
 * - `applyFilter`: Функция для применения выбранного фильтра к списку карточек поездок.
 */
export const useFilterTripData = (): IUseFilterTripData => {
  const [selectedFilter, setSelectedFilter] = useState<TripDataFilterSelectors>('all')

  /**
   * Карта типов фильтров и соответствующих им функций фильтрации.
   * Каждая функция фильтрует массив объектов `ITripCard` на основе определённых условий.
   */
  const filterMap: Record<TripDataFilterSelectors, (cards: ITripCard[]) => ITripCard[]> = {
    all: cards => cards,
    recommend: cards => cards.filter(card => card.recommend > 300).sort((a, b) => b.recommend - a.recommend),
    popular: cards => cards.filter(card => card.visited > 500),
    hotOffers: cards => cards.filter(card => card.isHotOffer),
    rating: cards => cards.filter(card => card.rating).sort((a, b) => b.rating - a.rating),
  }

  /**
   * Массив опций фильтров, которые могут быть выбраны пользователем.
   * @type {TripDataFilterSelectors[]}
   */
  const filterTripDataOptions: TripDataFilterSelectors[] = ['all', 'recommend', 'popular', 'hotOffers', 'rating']

  /**
   * Применяет выбранный фильтр к предоставленным карточкам поездок.
   *
   * @param {TripDataFilterSelectors} filter Выбранный тип фильтра.
   * @param {ITripCard[]} cards Массив карточек поездок, которые нужно отфильтровать.
   * @returns {ITripCard[]} Отфильтрованный массив карточек поездок.
   */
  const applyFilter = (filter: TripDataFilterSelectors, cards: ITripCard[]): ITripCard[] => {
    return filterMap[filter](cards)
  }

  return {
    selectedFilter,
    setSelectedFilter,
    filterTripDataOptions,
    applyFilter,
  }
}
