import { useQuery } from '@tanstack/react-query'
import axios, { AxiosResponse } from 'axios'
import { useSelector } from 'react-redux'

import { RootState } from 'app/store'

import { TTripCardData } from '../model'

interface IUseTripDataProps {
  search?: string
}

export const useTripData = ({ search }: IUseTripDataProps) => {
  const token = useSelector((state: RootState) => state.auth.token)
  const tripData = useQuery<AxiosResponse<TTripCardData>>({
    queryKey: ['tripCards', search, token],
    queryFn: () =>
      axios.get(`${process.env.REACT_APP_SERVER_API_URL}/tripCards`, {
        params: { search: search || undefined },
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }),
  })

  return tripData
}
