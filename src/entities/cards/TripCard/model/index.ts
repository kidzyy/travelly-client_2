export interface ITripCard {
  id: string
  tripName: string
  rating: number
  favorite: boolean
  isHotOffer: boolean
  price: number
  country: string
  imageUrl: string
  typeTrip: string[]
  region: string
  dateRange: number
  city: string
  passengers: number
  luggage: number
  facilities: string[]
  infoTrip: string
  recommend: number
  visited: number
}

export type TTripCardData = {
  tripCards: ITripCard[]
}
