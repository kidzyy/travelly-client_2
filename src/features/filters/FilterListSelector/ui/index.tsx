import style from './style.module.css'

interface FilterListSelectorProps<T> {
  filterOptions: T[]
  onFilterChange: (filter: T) => void
  selectedFilter: T // Добавляем текущий выбранный фильтр
}

const FilterListSelector = <T extends string | number>({
  filterOptions,
  onFilterChange,
  selectedFilter, // Добавляем для выделения активного фильтра
}: FilterListSelectorProps<T>) => {
  const handleClick = (filterOption: T) => {
    onFilterChange(filterOption)
  }

  return (
    <div className={style['filter-list-container']}>
      {filterOptions.map(option => (
        <button
          className={`${style['filter-button']} ${option === selectedFilter ? style['active'] : ''}`} // Применяем классы из CSS модуля
          key={option}
          onClick={() => handleClick(option)}
        >
          {`${(option as string)[0].toUpperCase()}${(option as string).slice(1)}`}
        </button>
      ))}
    </div>
  )
}

export default FilterListSelector
