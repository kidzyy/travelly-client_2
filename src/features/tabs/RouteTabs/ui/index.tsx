import { useState } from 'react'

import { Tab, TabList, TabPanel, Tabs } from 'react-tabs'

interface ITabsLabel {
  label: string
  icon: React.ReactNode
}
export type TDashboardPanelPages = React.ReactElement[]

interface IRouteTabsProps {
  tabs: ITabsLabel[]
  pages: TDashboardPanelPages
}

const RouteTabs = ({ tabs, pages }: IRouteTabsProps) => {
  const [selectedIndex, setSelectedIndex] = useState(0)

  return (
    <>
      <Tabs selectedIndex={selectedIndex} onSelect={index => setSelectedIndex(index)}>
        {pages.map((page, index) => (
          <TabPanel key={index}>{page}</TabPanel>
        ))}

        <TabList
          style={{
            borderRadius: '20px 20px 0 0',
            height: '70px',
            background: 'var(--primary-white)',
            width: '100%',
            position: 'fixed',
            bottom: 0,
            padding: '16px 0 0 0',
            display: 'flex',
            alignItems: 'flex-start',
            flexWrap: 'nowrap',
            boxShadow: '0 -2px 10px rgba(0, 0, 0, 0.1)',
            justifyContent: 'space-evenly',
          }}
        >
          {tabs.map((tab, index) => (
            <Tab
              key={index}
              style={{
                display: 'flex',
                flexBasis: selectedIndex === index ? '100%' : '0%',
                margin: '0 8px',
                justifyContent: 'center',
                alignItems: 'center',
                outline: 'none',
                padding: '2px 12px',
                height: '28px',
                borderRadius: '10px',
                opacity: selectedIndex === index ? 1 : 0.7,
                gap: '6px',
                transition: '0.1s ease-in-out all',
                backgroundColor: selectedIndex === index ? 'var(--secondary-orange)' : 'transparent',
                border: 'none',
                boxShadow: selectedIndex === index ? '0 4px 10px rgba(0, 0, 0, 0.15)' : 'none',
              }}
            >
              <span style={{ maxWidth: '24px', width: '100%', height: '24px' }}>{tab.icon}</span>
              {selectedIndex === index && (
                <span
                  style={{
                    transition: '0.3s ease ',
                    fontFamily: 'Poppins, sans-serif',
                    fontWeight: '600',
                    fontSize: '12px',
                    lineHeight: '120%',
                  }}
                >
                  {tab.label}
                </span>
              )}
            </Tab>
          ))}
        </TabList>
      </Tabs>
    </>
  )
}

export default RouteTabs
