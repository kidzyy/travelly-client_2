import { FC, useEffect } from 'react'

import { usePreloader } from 'shared/hooks/preloader'
import { StartPageArcIcon } from 'shared/svg/startPageLoaderComponents/StartPageArcIcon'
import { StartPagePlainIcon } from 'shared/svg/startPageLoaderComponents/StartPagePlainIcon'
import { MarkerLight } from 'shared/ui/dot/MarkerLight'

import style from './index.module.css'

import PreloaderProps from '../model/types'

const Preloader: FC<PreloaderProps> = ({ onProgressComplete }) => {
  const { progress, planeX, planeY, planeRotate } = usePreloader()

  const svgPathLength = 290.92
  const strokeOpacity = 1 + progress / 100 - 1

  useEffect(() => {
    progress === 100 && onProgressComplete()
  }, [progress, onProgressComplete])

  return (
    <div className={style['custom-loader']}>
      <MarkerLight styles={{ transform: 'translate(-120%, 150%)' }} />
      <StartPageArcIcon strokeOpacity={strokeOpacity} svgPathLength={svgPathLength} progress={progress} />
      <StartPagePlainIcon planeX={planeX} planeY={planeY} planeRotate={planeRotate} />
      <MarkerLight
        styles={{
          transform: 'translate(120%, -320%)',
          right: 0,
        }}
      />
    </div>
  )
}

export default Preloader
