interface PreloaderProps {
  onProgressComplete: () => void
}

export default PreloaderProps
