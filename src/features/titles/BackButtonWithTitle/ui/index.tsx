import { BackButton } from "features/buttons/BackButton"


const BackButtonWithTitle = ({ title }: { title: string }) => (
    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', position: 'relative', margin: '48px 0 24px 0' }}>
        <BackButton type='page' />
        <div style={{
            fontFamily: 'Poppins',
            fontWeight: 600,
            fontSize: 18,
            lineHeight: '144%',
            color: '#050505',
            position: 'absolute',
            top: 0,
            left: '50%',
            transform: 'translateX(-50%)'
        }}>
            {title}
        </div>
    </div>
)

export default BackButtonWithTitle