import SearchInput from './ui/SearchInput'

export * from './model/slice'
export { SearchInput }
