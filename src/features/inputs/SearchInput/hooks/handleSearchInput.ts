import { useEffect, useState } from 'react'

import { useDispatch, useSelector } from 'react-redux'
import { useLocation } from 'react-router-dom'

import { searchInputValueSelector } from '../model/selectors'
import { clearSearchInput, setSearchValue } from '../model/slice'

export const useHandleSearchInput = () => {
  const dispatch = useDispatch()
  const storedSearchValue = useSelector(searchInputValueSelector)
  const [searchValue, setLocalSearchValue] = useState(storedSearchValue) // Локальное состояние для инпута

  const location = useLocation()

  // Очищаем строку поиска при смене маршрута
  useEffect(() => {
    dispatch(clearSearchInput())
    setLocalSearchValue('') // Очистка локального состояния при смене маршрута
  }, [location, dispatch])

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setLocalSearchValue(e.target.value) // Обновляем локальное значение инпута
  }

  const handleClickSearchInputButton = () => {
    dispatch(setSearchValue(searchValue)) // Устанавливаем значение в Redux при клике на кнопку
  }

  return { searchValue, handleInputChange, handleClickSearchInputButton }
}
