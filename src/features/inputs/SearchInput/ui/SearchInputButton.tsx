import React from 'react'

import { SmallTouchButton } from 'shared/ui/buttons/SmallTouchButton'

interface ISearchInputButtonProps {
  onClick: () => void
}

export const SearchInputButton: React.FC<ISearchInputButtonProps> = ({ onClick }) => {
  return (
    <SmallTouchButton
      variation="extra_sm_card"
      onClick={onClick}
      styles={{
        background: 'var(--primary-orange)',
        borderRadius: '13px',
        padding: '6px',
        width: '36px',
        height: '36px',
        position: 'absolute',
        right: '3px',
        top: '50%',
        transform: 'translateY(-50%)',
      }}
    >
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="10" cy="10" r="6" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
        <path d="M14.5 14.5L19 19" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
      </svg>
    </SmallTouchButton>
  )
}
