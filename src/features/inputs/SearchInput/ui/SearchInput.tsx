import { Input } from 'shared/ui/inputs/Input'

import style from './style.module.css'

import { useHandleSearchInput } from '../hooks/handleSearchInput'
import { SearchInputButton } from './SearchInputButton'

const SearchInput = () => {
  const { searchValue, handleInputChange, handleClickSearchInputButton } = useHandleSearchInput()

  return (
    <label style={{ position: 'relative', display: 'inline-block', width: '100%' }}>
      <Input
        type="text"
        placeholder="Search"
        value={searchValue}
        className={style['search-input']}
        onChange={handleInputChange}
      />
      <SearchInputButton onClick={handleClickSearchInputButton} />
    </label>
  )
}

export default SearchInput
