import { PayloadAction, createSlice } from '@reduxjs/toolkit'

const initialState = {
  value: '',
}

const searchInputSlice = createSlice({
  name: 'searchInputSlice',
  initialState,
  reducers: {
    setSearchValue: (state, { payload }: PayloadAction<string>) => {
      state.value = payload
    },
    clearSearchInput: state => {
      state.value = ''
    },
  },
})

export const { setSearchValue, clearSearchInput } = searchInputSlice.actions
export default searchInputSlice.reducer
