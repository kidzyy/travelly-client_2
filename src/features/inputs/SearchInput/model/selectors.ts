import { RootState } from 'app/store'

export const searchInputValueSelector = (state: RootState) => state.searchInputSlice.value
