import { SmallTouchButton } from 'shared/ui/buttons/SmallTouchButton'

import { useFavoriteButtonMutate } from '../hooks'

interface IFavoriteButtonProps {
  isFavorite: boolean
  favoriteRequestPath: string
  tripCardId: string
}

const FavoriteButton = ({ favoriteRequestPath, isFavorite }: IFavoriteButtonProps) => {
  const { favoriteState, handleToggleFavorite } = useFavoriteButtonMutate({ isFavorite, favoriteRequestPath })

  return (
    <SmallTouchButton
      variation="extra_sm_card"
      styles={{ borderRadius: '7px', padding: '0px', backgroundColor: 'var(--primary-white)' }}
      onClick={() => handleToggleFavorite(!favoriteState)}
    >
      <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
          d="M6.26719 9.93288L10.0641 6.136C10.9969 5.1985 11.1328 3.66569 10.2516 2.686C10.0306 2.43912 9.76153 2.23988 9.46093 2.10048C9.16032 1.96107 8.83445 1.88442 8.50322 1.87521C8.17199 1.866 7.84236 1.92442 7.53447 2.0469C7.22658 2.16938 6.94689 2.35335 6.7125 2.58756L6 3.30475L5.38594 2.686C4.44844 1.75319 2.91563 1.61725 1.93594 2.4985C1.68906 2.71951 1.48982 2.98854 1.35042 3.28914C1.21101 3.58975 1.13436 3.91562 1.12515 4.24685C1.11594 4.57808 1.17435 4.90771 1.29683 5.2156C1.41931 5.52349 1.60328 5.80317 1.8375 6.03757L5.73282 9.93288C5.80391 10.0033 5.89994 10.0428 6 10.0428C6.10007 10.0428 6.19609 10.0033 6.26719 9.93288Z"
          stroke="var(--primary-red)"
          strokeLinecap="round"
          strokeLinejoin="round"
          fill={favoriteState ? 'var(--primary-red)' : 'var(--primary-white)'} // Меняем заливку в зависимости от состояния избранного
        />
      </svg>
    </SmallTouchButton>
  )
}
export default FavoriteButton
