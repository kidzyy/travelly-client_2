import { useCallback, useState } from 'react'

import { useMutation } from '@tanstack/react-query'
import axios from 'axios'
import { debounce } from 'lodash'
import { useSelector } from 'react-redux'

import { RootState } from 'app/store'
import { queryClient } from 'app/ui/App'

interface IUseFavoriteButtonMutate {
  isFavorite: boolean
  favoriteRequestPath: string
}

/**
 * Хук для управления состоянием кнопки "Избранное" с дебаунсом.
 *
 * @param {IUseFavoriteButtonMutate} param - Объект с параметрами:
 * @param {boolean} param.isFavorite - Начальное состояние кнопки "Избранное".
 * @param {string} param.favoriteRequestPath - Путь API для отправки запроса на изменение состояния.
 * @returns Объект с состоянием кнопки и функцией для переключения состояния.
 */
export const useFavoriteButtonMutate = ({ isFavorite, favoriteRequestPath }: IUseFavoriteButtonMutate) => {
  const [favoriteState, setFavoriteState] = useState<boolean>(isFavorite)
  const token = useSelector((state: RootState) => state.auth.token)

  const toggleFavoriteRequest = async (favorite: boolean) => {
    const response = await axios.put(
      `${process.env.REACT_APP_SERVER_API_URL}/${favoriteRequestPath}`,
      { favorite },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )
    return response.data
  }

  const { mutate } = useMutation<boolean, Error, boolean>({
    mutationFn: (favorite: boolean) => toggleFavoriteRequest(favorite),
    onMutate: favorite => setFavoriteState(favorite),
    onError: () => setFavoriteState(prev => !prev),
    onSettled: () => {
      queryClient.invalidateQueries({
        queryKey: ['tripCards'],
      })
    },
  })

  const debouncedMutate = useCallback(
    debounce((value: boolean) => {
      mutate(value)
    }, 300),
    [],
  )

  const handleToggleFavorite = (value: boolean) => {
    setFavoriteState(value)
    debouncedMutate(value)
  }

  return {
    favoriteState,
    handleToggleFavorite,
  }
}
