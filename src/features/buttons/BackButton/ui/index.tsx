import { FC } from 'react'

import style from './index.module.css'

import useGoBack from '../lib/useGoBack'
import IBackButton from '../model/types'

const BackButton: FC<IBackButton> = ({ styles, type }) => {
  const { goBack } = useGoBack({ type })

  return (
    <button onClick={goBack} className={style['back-button']} style={{ ...styles }}>
      <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
          d="M20.6666 25.3333L12.0404 16.7071C11.6498 16.3166 11.6498 15.6834 12.0404 15.2929L20.6666 6.66667"
          stroke="var(--primary-black)"
          strokeWidth="3"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    </button>
  )
}

export default BackButton
