import { useNavigate } from 'react-router-dom'

import { useNextStepHandler } from 'shared/hooks/nextStepHandler'

import IBackButton from '../model/types'

const useGoBack = ({ type }: IBackButton) => {
  const navigate = useNavigate()
  const { currentStep, setCurrentStep } = useNextStepHandler()

  const goBack = () => {
    if (type === 'page') {
      navigate(-1)
    } else if (type === 'trackBar') {
      setCurrentStep(currentStep - 1)
    }
  }

  return { goBack }
}

export default useGoBack
