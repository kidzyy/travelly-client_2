import { IButton } from 'shared/ui/buttons/Button'

interface IBackButton extends IButton {
  type?: 'trackBar' | 'page'
}

export default IBackButton
