import { lazy } from 'react'

const BackButton = lazy(() => import('features/buttons/BackButton/ui'))

export { BackButton }
