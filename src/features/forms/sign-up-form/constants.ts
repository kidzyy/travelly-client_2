import * as Yup from 'yup'

export const validationSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, 'Minimum of 2 characters')
    .max(50, 'Maximum of 50 characters')
    .required('Required Field'),
  email: Yup.string().email().required(),
  lastName: Yup.string().min(2, 'Minimum of 2 characters').max(50, 'Максимум 50 символов').required('Required Field'),
  password: Yup.string().min(8, 'Minimum of 8 characters').required('Required Field'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password'), undefined], 'Passwords must match')
    .required('Password confirmation is required'),
})

export const formikInitialStateSignUp: Record<
  'firstName' | 'lastName' | 'email' | 'password' | 'confirmPassword',
  string
> = {
  firstName: '',
  lastName: '',
  email: '',
  password: '',
  confirmPassword: '',
}
