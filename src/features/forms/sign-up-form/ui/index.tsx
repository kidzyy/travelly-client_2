import { FC } from 'react'

import { useMutation } from '@tanstack/react-query'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import toast from 'react-hot-toast'
import { useNavigate } from 'react-router-dom'

import { Fetcher } from 'shared/api/fetchData'
import { MainButton } from 'shared/ui/buttons/MainButton'

import styles from './index.module.css'

import { formikInitialStateSignUp, validationSchema } from '../constants'

const stylesSignUpSteps: React.CSSProperties = {
  backgroundColor: 'var(--primary-orange)',
  color: 'var(--primary-white)',
  marginTop: '1rem',
  marginBottom: '3rem',
} as const

const SignUpForm: FC = () => {
  const navigate = useNavigate()
  const fetcher = new Fetcher(process.env.REACT_APP_SERVER_API_URL!)

  const { mutate } = useMutation({
    mutationFn: (body: any) => fetcher.post('/register', body),
    onMutate: () => {
      const toastId = toast.loading(<p style={{ fontFamily: 'Poppins' }}>Checking the data...</p>)

      return { toastId }
    },
    onSuccess: (_, __, context) => {
      if (context?.toastId) {
        toast.dismiss(context.toastId)
      }
      toast.success(<p style={{ fontFamily: 'Poppins' }}>User has been successfully registered</p>)
      navigate('/log-in')
    },
    onError: (_, __, context) => {
      if (context?.toastId) {
        toast.dismiss(context.toastId)
      }
      toast.error(<p style={{ fontFamily: 'Poppins' }}>Error saving user... Try Again!</p>)
    },
  })

  return (
    <>
      <Formik
        initialValues={formikInitialStateSignUp}
        validationSchema={validationSchema}
        onSubmit={(values, { setSubmitting }) => {
          mutate(values, {
            onSettled: () => {
              setSubmitting(false)
            },
          })
        }}
      >
        {({ submitForm, isSubmitting, errors, touched }) => (
          <Form style={{ display: 'flex', flexDirection: 'column', gap: '2rem', marginTop: '3rem' }}>
            <div style={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}>
              <div>
                <Field
                  type="text"
                  name="firstName"
                  className={`${styles['input-main']} ${touched.firstName && errors.firstName ? styles['error'] : ''}`}
                  placeholder="First Name"
                />
                <ErrorMessage name="firstName" component="div" className={styles.errorMessage} />
              </div>

              <div>
                <Field
                  type="text"
                  name="lastName"
                  className={`${styles['input-main']} ${touched.lastName && errors.lastName ? styles['error'] : ''}`}
                  placeholder="Last Name"
                />
                <ErrorMessage name="lastName" component="div" className={styles.errorMessage} />
              </div>

              <div>
                <Field
                  type="text"
                  name="email"
                  className={`${styles['input-main']} ${touched.email && errors.email ? styles['error'] : ''}`}
                  placeholder="Email"
                />
                <ErrorMessage name="email" component="div" className={styles.errorMessage} />
              </div>
            </div>

            <div style={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}>
              <div>
                <Field
                  type="password"
                  name="password"
                  className={`${styles['input-main']} ${touched.password && errors.password ? styles['error'] : ''}`}
                  placeholder="Password"
                />
                <ErrorMessage name="password" component="div" className={styles.errorMessage} />
              </div>

              <div>
                <Field
                  type="password"
                  name="confirmPassword"
                  className={`${styles['input-main']} ${
                    touched.confirmPassword && errors.confirmPassword ? styles['error'] : ''
                  }`}
                  placeholder="Confirm Password"
                />
                <ErrorMessage name="confirmPassword" component="div" className={styles.errorMessage} />
              </div>
            </div>

            <MainButton
              type={'button'}
              variation={'large'}
              styles={stylesSignUpSteps}
              onClick={submitForm}
              disabled={isSubmitting}
            >
              {isSubmitting ? 'Loading...' : 'Sign Up'}
            </MainButton>
          </Form>
        )}
      </Formik>
    </>
  )
}

export default SignUpForm
