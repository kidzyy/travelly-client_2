import { FC } from 'react'

import { ErrorMessage, Field, Form, Formik } from 'formik'

import { MainButton } from 'shared/ui/buttons/MainButton'

import styles from './index.module.css'

//@ts-ignore
import { useSignIn } from '../api'
import { formikInitialStateSignUp, validationSchema } from '../constants'

const stylesSignUpSteps: React.CSSProperties = {
  backgroundColor: 'var(--primary-orange)',
  color: 'var(--primary-white)',
  marginTop: '1rem',
  marginBottom: '3rem',
} as const

const SignInForm: FC = () => {
  const { mutate } = useSignIn()

  return (
    <Formik
      initialValues={formikInitialStateSignUp}
      validationSchema={validationSchema}
      onSubmit={(values, { setSubmitting }) => {
        mutate(values, {
          onSettled: () => {
            setSubmitting(false)
          },
        })
      }}
    >
      {({ submitForm, isSubmitting, errors, touched }) => (
        <Form style={{ display: 'flex', flexDirection: 'column', gap: '2rem', marginTop: '3rem' }}>
          <div style={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}>
            <Field
              type="text"
              name="email"
              className={`${styles['input-main']} ${touched.email && errors.email ? styles['error'] : ''}`}
              placeholder="Email"
            />
            <ErrorMessage name="email" component="div" className={styles.errorMessage} />
          </div>

          <div style={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}>
            <Field
              type="password"
              name="password"
              className={`${styles['input-main']} ${touched.password && errors.password ? styles['error'] : ''}`}
              placeholder="Password"
            />
            <ErrorMessage name="password" component="div" className={styles.errorMessage} />
          </div>

          <MainButton
            type={'button'}
            variation={'large'}
            styles={stylesSignUpSteps}
            onClick={submitForm}
            disabled={isSubmitting}
          >
            {isSubmitting ? 'Loading...' : 'Sign In'}
          </MainButton>
        </Form>
      )}
    </Formik>
  )
}

export default SignInForm
