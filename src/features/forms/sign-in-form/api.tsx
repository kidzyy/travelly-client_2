import { useMutation } from '@tanstack/react-query'
import { toast } from 'react-hot-toast'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import { Fetcher } from 'shared/api/fetchData'

import { login } from 'entities/users/authSlice'

export const useSignIn = () => {
  const navigate = useNavigate()
  const fetcher = new Fetcher(process.env.REACT_APP_SERVER_API_URL!)
  const dispatch = useDispatch()

  const { mutate } = useMutation({
    mutationFn: async (body: any) => fetcher.post('/login', body),
    onMutate: () => {
      const toastId = toast.loading(<p style={{ fontFamily: 'Poppins' }}>Checking the data...</p>)
      return { toastId }
    },
    onSuccess: (data, __, context) => {
      if (context?.toastId) {
        toast.dismiss(context.toastId)
      }
      toast.success(<p style={{ fontFamily: 'Poppins' }}>User has been successfully logged in</p>)
      dispatch(login(data))
      navigate('/dashboard')
    },
    onError: (_, __, context) => {
      if (context?.toastId) {
        toast.dismiss(context.toastId)
      }
      toast.error(<p style={{ fontFamily: 'Poppins' }}>Login failed... Try Again!</p>)
    },
  })

  return { mutate }
}
