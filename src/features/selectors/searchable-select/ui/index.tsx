
import Select from 'react-select'
import styles from './styles.module.css'
import { useState } from 'react'
import moment from 'moment'
import { PassengerLabel } from 'shared/ui/labels/PassengerLabel'
import { FormikErrors } from 'formik'
import { TFormikStateSearchableBookingFormRecord } from 'widgets/searchables/SearchableBookingForm/searchable-trips-form/ui'
import { dateOptions, regionSelectOptions, customStyles, locationSelectOptions, numberOptions, customNumberSelectStyles } from '../constants'

type TSetFieldValueSearchableSelect = (field: string, value: any, shouldValidate?: boolean) => Promise<void | FormikErrors<TFormikStateSearchableBookingFormRecord>>
type TTypeSearchableSelect = 'region' | 'location' | 'flightDate' | 'passenger'


const SearchableSelect = ({ type, setFieldValue, values }: { type: TTypeSearchableSelect, setFieldValue: TSetFieldValueSearchableSelect, values: TFormikStateSearchableBookingFormRecord }) => {

    const [selectedNumber, setSelectedNumber] = useState<{ luggage: number; passenger: number }>({
        luggage: 0,
        passenger: 0,
    })
    const [filteredDepartureOptions, setFilteredDepartureOptions] = useState(dateOptions)

    if (type === 'region') {
        return <div>
            <label className={styles['input-select__label']} htmlFor="location">
                Region
            </label>
            <Select
                name={type}
                options={regionSelectOptions}
                styles={customStyles}
                onChange={option => setFieldValue(type, option?.value)}
            />
        </div>

    }

    if (type === 'location') {
        return <div>
            <label className={styles['input-select__label']} htmlFor="location">
                Location
            </label>
            <Select
                name={type}
                options={locationSelectOptions}
                styles={customStyles}
                onChange={option => setFieldValue(type, option?.value)}
            />
        </div>
    }

    if (type === 'flightDate') {
        return <div style={{ display: 'flex', gap: '1rem' }}>
            <div style={{ flex: '1' }}>
                <label className={styles['input-select__label']} htmlFor="location">
                    Arrival
                </label>
                <Select
                    name="arrival"
                    options={dateOptions}
                    styles={customStyles}
                    value={dateOptions.find(option => option.value === values.arrival)}
                    onChange={selectedArrival => {
                        setFieldValue('arrival', selectedArrival?.value)
                        setFieldValue('departure', null)
                        setFilteredDepartureOptions([])

                        const newFilteredOptions = dateOptions.filter(option =>
                            moment(option.value).isSameOrAfter(selectedArrival?.value),
                        )
                        setFilteredDepartureOptions(newFilteredOptions)
                    }}
                />
            </div>

            <div style={{ flex: '1' }}>
                <label className={styles['input-select__label']} htmlFor="location">
                    Departure
                </label>
                <Select
                    name="departure"
                    options={dateOptions}
                    styles={customStyles}
                    value={filteredDepartureOptions.find(option => option.value === values.departure) || null}
                    onChange={option => setFieldValue('departure', option?.value)}
                    isOptionDisabled={option => moment(option.value).isBefore(values.arrival)}
                    isDisabled={!values.arrival}
                />
            </div>
        </div>
    }


    if (type === 'passenger') {
        return <div style={{ marginTop: '1rem' }}>
            <label
                style={{
                    display: 'block',
                    fontFamily: 'Poppins,sans-serif',
                    fontWeight: 600,
                    fontSize: '14px',
                    lineHeight: '143%',
                    color: '#727272',
                }}
            >
                Passenger & Luggage
            </label>
            <div style={{ display: 'flex', gap: '24px' }}>
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        maxWidth: '56px',
                        width: '100%',
                    }}
                >
                    <div
                        style={{
                            position: 'relative',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            maxWidth: '56px',
                            width: '100%',
                        }}
                    >
                        <PassengerLabel type='adult'/>
                        <Select
                            name="passenger"
                            value={numberOptions.find(option => option.value === values.passenger)}
                            options={numberOptions}
                            styles={customNumberSelectStyles}
                            onChange={option => {
                                setFieldValue('passenger', option?.value)
                                setSelectedNumber(prevState => ({
                                    ...prevState,
                                    passenger: option?.value!,
                                }))
                            }}
                        />
                    </div>

                    <hr
                        style={{
                            width: '56px',
                            borderTop: '1px',
                            borderColor: selectedNumber.passenger > 0 ? '#01635D' : '#ccc',
                        }}
                    />
                </div>

                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        maxWidth: '56px',
                        width: '100%',
                    }}
                >
                    <div
                        style={{
                            position: 'relative',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            maxWidth: '56px',
                            width: '100%',
                        }}
                    >
                        <PassengerLabel type='child' selectedNumber={selectedNumber} />
                        <Select
                            name="luggage"
                            value={numberOptions.find(option => option.value === values.luggage)}
                            options={numberOptions}
                            styles={customNumberSelectStyles}
                            onChange={option => {
                                setFieldValue('luggage', option?.value)
                                setSelectedNumber(prevState => ({
                                    ...prevState,
                                    luggage: option?.value!,
                                }))
                            }}
                        />
                    </div>

                    <hr
                        style={{
                            width: '56px',
                            borderTop: '1px',
                            borderColor: selectedNumber.luggage > 0 ? '#01635D' : '#ccc',
                        }}
                    />
                </div>
            </div>
        </div>
    }

    return null
}

export default SearchableSelect