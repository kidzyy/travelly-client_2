import moment from "moment"

// styles
const customStyles = {
    control: (base: any, state: any) => ({
        ...base,
        outline: 'none',
        border: 'none',
        borderRadius: '20px',
        width: '100%',
        height: '54%',
        padding: '20px 10px 0px 7px',
        appearance: 'none',
        fontFamily: 'Poppins, sans-serif',
        fontWeight: 400,
        fontSize: '16px',
        lineHeight: '137%',
        color: 'black',
        backgroundColor: state.isDisabled ? '#f2f2f2' : 'white', // Серый фон для disabled
        ...(state.isFocused && {
            borderColor: 'none',
            boxShadow: 'none',
        }),
    }),
    singleValue: (provided: any, state: any) => ({
        ...provided,
        color: state.isDisabled ? '#888' : '#000', // Цвет для disabled
    }),

    option: (base: any, state: any) => ({
        ...base,

        fontFamily: 'Poppins, sans-serif',
        fontWeight: 400,
        fontSize: '14px',
        lineHeight: '137%',
        padding: '10px',
        borderRadius: '20px',
        backgroundColor: state.isDisabled ? '#f2f2f2' : 'none', // Изменение фона для disabled
        color: state.isDisabled ? '#ccc' : state.isSelected ? '#fff' : '#333', // Изменение цвета текста для disabled
        ...(state.isFocused && {
            borderColor: '#ccc',
            boxShadow: 'none',
        }),
        ...(state.isSelected && {
            backgroundColor: 'var(--primary-green)',
        }),
    }),
    menu: (provided: any) => ({
        ...provided,
        backgroundColor: '#f9f9f9',
        borderRadius: '20px',
        border: 'none',
        padding: '8px',
        maxHeight: '150px',
        overflowY: 'auto',
        zIndex: '2',
    }),
    dropdownIndicator: () => ({
        display: 'none',
    }),
    indicatorSeparator: () => ({
        display: 'none',
    }),
}

const customNumberSelectStyles = {
    control: (base: any, state: any) => ({
        ...base,
        display: 'flex',
        alignItems: 'center',
        backgroundColor: 'transparent',
        boxShadow: 'none',
        border: 'none',
        color: '#01635D',
        width: '30px',
    }),
    singleValue: (provided: any, state: any) => ({
        ...provided,
        color: state.selectProps.value.value > 0 ? '#01635D' : '#ccc', // Цвет числа
        fontWeight: 500,
        fontFamily: 'Poppins,sans-serif',
        lineHeight: '100%',
        textAlign: 'center',
        flex: '1',
    }),
    option: (base: any, state: any) => ({
        ...base,

        fontFamily: 'Poppins, sans-serif',
        fontWeight: 400,
        fontSize: '14px',
        lineHeight: '137%',
        padding: '10px',
        borderRadius: '20px',
        backgroundColor: state.isDisabled ? '#f2f2f2' : 'none', // Изменение фона для disabled
        color: state.isDisabled ? '#ccc' : state.isSelected ? '#fff' : '#333', // Изменение цвета текста для disabled
        ...(state.isFocused && {
            borderColor: '#ccc',
            boxShadow: 'none',
        }),
        ...(state.isSelected && {
            backgroundColor: 'var(--primary-green)',
        }),
    }),
    menu: (provided: any) => ({
        ...provided,
        backgroundColor: '#f9f9f9',
        borderRadius: '20px',
        border: 'none',
        padding: '8px',
        maxHeight: '150px',
        overflowY: 'auto',
        zIndex: '2',
        width: '70px',
    }),
    dropdownIndicator: () => ({
        display: 'none',
    }),
    indicatorSeparator: () => ({
        display: 'none',
    }),
}

const locationSelectOptions = [
    { value: 'Italy', label: 'Italy' },
    { value: 'France', label: 'France' },
    { value: 'Great Britain', label: 'Great Britain' },
    { value: 'USA', label: 'USA' },
    { value: 'Japan', label: 'Japan' },
    { value: 'Argentina', label: 'Argentina' },
    { value: 'Australia', label: 'Australia' },
    { value: 'South Africa', label: 'South Africa' },
    { value: 'Canada', label: 'Canada' },
    { value: 'Spain', label: 'Spain' },
    { value: 'Thailand', label: 'Thailand' },
    { value: 'Netherlands', label: 'Netherlands' },
    { value: 'Brazil', label: 'Brazil' },
    { value: 'United Arab Emirates', label: 'United Arab Emirates' },
    { value: 'Austria', label: 'Austria' },
    { value: 'Morocco', label: 'Morocco' },
    { value: 'New Zealand', label: 'New Zealand' },
    { value: 'Czech Republic', label: 'Czech Republic' },
    { value: 'Turkey', label: 'Turkey' },
]

const generateDates = (daysAhead: any) => {
    const dates = []
    for (let i = 0; i <= daysAhead; i++) {
        const date = moment().add(i, 'days').format('YYYY-MM-DD')
        const formattedDate = moment(date).format('MMM DD, YYYY') // Форматируем как "Oct 10, 2024"
        dates.push({ value: date, label: formattedDate })
    }
    return dates
}

// options
const regionSelectOptions = [
    { value: 'Europe', label: 'Europe' },
    { value: 'North America', label: 'North America' },
    { value: 'Asia', label: 'Asia' },
    { value: 'South America', label: 'South America' },
    { value: 'Oceania', label: 'Oceania' },
    { value: 'Africa', label: 'Africa' },
    { value: 'Australia', label: 'Australia' },
    { value: 'South Africa', label: 'South Africa' },
    { value: 'North America', label: 'North America' },
]
const numberOptions = [
    { value: 0, label: '0' },
    { value: 1, label: '1' },
    { value: 2, label: '2' },
    { value: 3, label: '3' },
    { value: 4, label: '4' },
    { value: 5, label: '5' },
    { value: 6, label: '6' },
    { value: 7, label: '7' },
    { value: 8, label: '8' },
]
const dateOptions = generateDates(30)

export { customStyles, customNumberSelectStyles, locationSelectOptions, regionSelectOptions, numberOptions, dateOptions }