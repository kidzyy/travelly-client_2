import { BaseStyle } from './types'

export const generateTextStyle = (
  fontSize: string,
  lineHeight: string,
  fontWeight: number,
  color: string,
): BaseStyle => ({
  color: color,
  fontFamily: 'Poppins',
  fontSize,
  fontWeight: fontWeight,
  fontStyle: 'normal',
  lineHeight,
})
