export interface IFontsOptions {
  regular: {
    [key: string]: BaseStyle
  }
  medium: {
    [key: string]: BaseStyle
  }
  semiBold: {
    [key: string]: BaseStyle
  }
}

export interface BaseStyle {
  color: string
  fontFamily: string
  fontSize: string
  fontStyle: string
  fontWeight: number
  lineHeight: string
}
