import { IFontsOptions } from './types'
import { generateTextStyle } from './utils'

export const greyFontsOptions: IFontsOptions = {
  regular: {
    extraSmall_grey: generateTextStyle('10px', '16px', 400, 'var(--primary-grey)'),
    small_grey: generateTextStyle('12px', '18px', 400, 'var(--primary-grey)'),
    smallMedium_grey: generateTextStyle('14px', '20px', 400, 'var(--primary-grey)'),
    medium_grey: generateTextStyle('16px', '22px', 400, 'var(--primary-grey)'),
  },
  medium: {
    smallMedium_grey: generateTextStyle('14px', '20px', 500, 'var(--primary-grey)'),
    medium_grey: generateTextStyle('16px', '12px', 500, 'var(--primary-grey)'),
  },
  semiBold: {
    smallMedium_grey: generateTextStyle('14px', '20px', 600, 'var(--primary-grey)'),
  },
}

export const blackFontsOptions: IFontsOptions = {
  regular: {
    medium_black: generateTextStyle('16px', '22px', 400, 'var(--primary-black)'),
    small_black: generateTextStyle('12px', '18px', 400, 'var(--primary-black)'),
    smallMd_black: generateTextStyle('14px', '20px', 400, 'var(--primary-black)'),
    smallMd_smLineH_black: generateTextStyle('14px', '16px', 400, 'var(--primary-black)'),
    smallMd_mLineH_black: generateTextStyle('14px', '22px', 400, 'var(--primary-black)'),
  },
  medium: {
    smallMd_black: generateTextStyle('14px', '20px', 500, 'var(--primary-black)'),
    medium_black: generateTextStyle('16px', '24px', 500, 'var(--primary-black)'),
    mediumLg_black: generateTextStyle('18px', '26px', 500, 'var(--primary-black)'),
    extraLg_black: generateTextStyle('32px', '34px', 500, 'var(--primary-black)'),
  },
  semiBold: {
    small_black: generateTextStyle('12px', '14px', 600, 'var(--primary-black)'),
    smallMd_black: generateTextStyle('14px', '20px', 600, 'var(--primary-black)'),
    largeSm_black: generateTextStyle('20px', '28px', 600, 'var(--primary-black)'),
    mediumLg_black: generateTextStyle('18px', '26px', 600, 'var(--primary-black)'),
    medium_mLineH_black: generateTextStyle('16px', '22px', 600, 'var(--primary-black)'),
    medium_mLgLineH_black: generateTextStyle('16px', '24px', 600, 'var(--primary-black)'),
    largeMd_black: generateTextStyle('24px', '32px', 600, 'var(--primary-black)'),
  },
}
