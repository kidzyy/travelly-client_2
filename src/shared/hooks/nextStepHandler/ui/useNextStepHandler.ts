import { useEffect, useState } from 'react'

import { useDispatch, useSelector } from 'react-redux'

import { selectCurrentStep, setCurrentStep } from 'shared/ui/trackbar/TrackBar/api/trackBarSlice'

import IUseNextStepHandler from '../model/types'

const useNextStepHandler = <T>(): IUseNextStepHandler<T> => {
  const [finalStep, setFinalStep] = useState<number>(0)

  const dispatch = useDispatch()
  const currentStep = useSelector(selectCurrentStep)

  const handleNextStepClick = (data: T[]): void => {
    if (data) {
      dispatch(setCurrentStep(currentStep < data.length - 1 ? currentStep + 1 : 0))
    }
  }

  useEffect(() => {
    setFinalStep(prev => prev + 1)
  }, [currentStep])

  return {
    currentStep,
    handleNextStepClick,
    finalStep,
    setCurrentStep: (step: number) => dispatch(setCurrentStep(step)),
  }
}

export default useNextStepHandler
