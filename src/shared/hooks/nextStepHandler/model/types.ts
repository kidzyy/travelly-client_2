interface IUseNextStepHandler<T> {
  currentStep: number
  handleNextStepClick: (data: T[]) => void
  finalStep: number
  setCurrentStep: (initialStep: number) => void
}

export default IUseNextStepHandler
