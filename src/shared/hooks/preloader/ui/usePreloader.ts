import { useEffect, useState } from 'react'

import { IPreloaderState } from '../model/types'
import simulateLoading from '../utils/simulateLoading'

const usePreloader = (): IPreloaderState => {
  const [progress, setProgress] = useState<number>(0)
  const [planeX, setPlaneX] = useState<number>(-2)
  const [planeY, setPlaneY] = useState<number>(-25)
  const [planeRotate, setPLaneRotate] = useState<number>(-14)

  useEffect(() => {
    simulateLoading({ setProgress, setPlaneX, setPlaneY, setPLaneRotate })
  }, [])

  return {
    progress,
    planeX,
    planeY,
    planeRotate,
  }
}

export default usePreloader
