export interface IPreloaderState {
  progress: number
  planeX: number
  planeY: number
  planeRotate: number
}
export interface IPreloaderActions {
  setProgress: (progress: number) => void
  setPlaneX: (planeX: number) => void
  setPlaneY: (planeY: number) => void
  setPLaneRotate: (rotate: number) => void
}
