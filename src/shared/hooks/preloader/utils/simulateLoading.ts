import { IPreloaderActions } from '../model/types'

const simulateLoading = ({ setProgress, setPlaneX, setPlaneY, setPLaneRotate }: IPreloaderActions) => {
  let currentProgress = 0
  let currentPlaneX = -2
  let currentPlaneY = -25
  let rotatePlane = -14

  const targetY = -46
  const increaseYValue = 0.1
  const decreaseYValue = 0.1

  const animate = () => {
    currentProgress += 1
    currentPlaneX += 2.5
    rotatePlane += 0.37

    const deltaY = targetY - currentPlaneY

    if (Math.abs(deltaY) > 0.1 && currentProgress < -targetY) {
      currentPlaneY += deltaY * 0.05
    } else {
      currentPlaneY -= deltaY * 0.04
    }

    if (currentProgress > -targetY) {
      currentPlaneY += increaseYValue
    } else {
      currentPlaneY -= decreaseYValue
    }

    setProgress(currentProgress)
    setPlaneX(currentPlaneX)
    setPlaneY(currentPlaneY)
    setPLaneRotate(rotatePlane)

    if (currentProgress < 100) {
      requestAnimationFrame(animate)
    }
  }

  requestAnimationFrame(animate)
}

export default simulateLoading
