import { FC } from 'react'

import styles from './layout.module.css'

interface IChildrenProps {
  children?: React.ReactNode
  style?: React.CSSProperties
}

const Layout: FC<IChildrenProps> = ({ children, style }) => {
  return (
    <div className={styles['container-main']} style={style}>
      {children}
    </div>
  )
}

export default Layout
