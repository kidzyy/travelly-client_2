import { useState } from "react"


interface IButtonsListProps {
    buttonsList: {
        title:string
        id:number
    }[]
    title?: string
    onClick: (field: string, value: string) => void;
}

const SelectorButtons = ({ buttonsList, title, onClick }: IButtonsListProps) => {
    
    const [selected, setSelected] = useState<number>()

    return (
        <>
            <div style={{ fontFamily: 'Poppins,sans-serif', fontWeight: '500', fontSize: '14px', color: '#727272' }}>{title}</div>
            <div style={{ display: 'flex', width: '100%', overflowX: 'scroll', gap: '8px', alignItems: 'center' }}>
                {buttonsList.map((buttonItem,index) => (
                    <button key={index} style={{
                        maxHeight: '36px',
                        display: 'flex',
                        flex: 1,
                        backgroundColor: `${selected === buttonItem.id ? '#089083' : '#fff'}`,
                        borderRadius: '8px',
                        alignItems: 'center',
                        padding: '8px',
                        textWrap: 'nowrap',
                        fontFamily: 'Poopins,sans-serif',
                        fontWeight: 400,
                        fontSize: '14px',
                        border: 'none',
                        color: `${selected === buttonItem.id ? '#fff' : '#089083'}`,
                        transition:'0.1s ease-out'
                    }}
                        onClick={(e) => {
                            e.preventDefault()
                            setSelected(buttonItem.id)
                            onClick('selectorType', buttonItem.title);
                        }}
                    >
                        {buttonItem.title}
                    </button>
                ))}
            </div>
        </>

    )
}

export default SelectorButtons