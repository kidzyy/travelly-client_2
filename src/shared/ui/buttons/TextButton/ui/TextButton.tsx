import { FC } from 'react'

import { IButton } from '../../Button'

const TextButton: FC<IButton> = ({ styles, onClick, children }) => {
  const stylesButton = { ...styles, border: 'none', backgroundColor: 'transparent' }
  return (
    <button style={stylesButton} onClick={onClick}>
      {children}
    </button>
  )
}

export default TextButton
