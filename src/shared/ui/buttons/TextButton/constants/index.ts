const styleTextButtonGreen = {
  color: 'var(--green-700, #01635D)',
  display: 'inline-block',
  textAlign: 'center',
  fontFamily: 'Poppins, sans-serif',
  fontSize: '16px',
  fontStyle: 'normal',
  fontWeight: 500,
  lineHeight: '24px',
} as const

export default styleTextButtonGreen