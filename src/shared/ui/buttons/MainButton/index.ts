import { lazy } from 'react'

import mainButtonStyles from './constants'

const MainButton = lazy(() => import('shared/ui/buttons/MainButton/ui/MainButton'))
export { MainButton, mainButtonStyles }
