const mainButtonStyles = {
  large: {
    width: '21.4375rem',
  },
  medium: {
    width: '14.5625rem',
  },
  small: {
    width: '12.875rem',
  },
  extraSmall: {
    width: '10.21875rem',
  },
}

export default mainButtonStyles
