import { FC } from 'react'

import style from './index.module.css'

import { IButton } from '../../Button'
import mainButtonStyles from '../constants'

interface IMainButton extends IButton {
  variation: 'large' | 'medium' | 'small' | 'extraSmall'
  type?: 'submit' | 'reset' | 'button' | undefined
}

const MainButton: FC<IMainButton> = props => {
  const { children, variation, styles, onClick, type } = props
  const selectedStyle = mainButtonStyles[variation] || mainButtonStyles.large

  return (
    <button
      type={type}
      onClick={onClick}
      className={style['button-main']}
      style={{ ...selectedStyle, ...styles }}
      {...props}
    >
      {children}
    </button>
  )
}

export default MainButton
