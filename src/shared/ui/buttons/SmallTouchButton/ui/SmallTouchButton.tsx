import { FC } from 'react'

import style from './index.module.css'

import { IButton } from '../../Button'
import { smallTouchButtonStyles } from '../constants'

interface ISmallTouchButton extends IButton {
  children?: React.ReactNode
  variation: 'sm' | 'extra_sm' | 'extra_sm_card'
}

const SmallTouchButton: FC<ISmallTouchButton> = ({ children, variation, styles, onClick }) => {
  const selectedStyle = smallTouchButtonStyles[variation]
  return (
    <button onClick={onClick} className={style['small-touch-button']} style={{ ...selectedStyle, ...styles }}>
      {children}
    </button>
  )
}

export default SmallTouchButton
