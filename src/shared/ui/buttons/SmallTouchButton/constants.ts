type ISmallTouchButtonStyles = Record<string, { width: string; height: string }>

export const smallTouchButtonStyles: ISmallTouchButtonStyles = {
  sm: {
    width: '3.75rem',
    height: '3.75rem',
  },
  extra_sm: {
    width: '3.125rem',
    height: '3.125rem',
  },
  extra_sm_card: {
    width: '1.25rem',
    height: '1.25rem',
  },
}