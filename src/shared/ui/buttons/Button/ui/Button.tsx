import { FC } from 'react'

import IButton from '../model/types'

const Button: FC<IButton> = ({ styles, onClick, children }) => {
  return (
    <button style={{ ...styles }} onClick={onClick}>
      {children}
    </button>
  )
}

export default Button
