import IButton from './model/types'
import Button from './ui/Button'

export { Button }
export type { IButton }
