import { CSSProperties, HTMLAttributes } from 'react'

interface IButton extends HTMLAttributes<HTMLButtonElement> {
  styles?: CSSProperties
  onClick?: () => void
  children?: React.ReactNode
  disabled?: boolean
}

export default IButton
