import React from 'react'

import ContentLoader from 'react-content-loader'

import { getSizeCard } from '../utils/getSizeCard'

// Функция для извлечения числового значения из строки с пикселями
const extractNumber = (size: string) => parseFloat(size.replace('px', ''))

// Компонент для отображения скелетона карточки
const StandardCardSkeleton: React.FC<{ typeSizeCard: 'small' | 'middle' | 'flexGrow' }> = ({ typeSizeCard }) => {
  const sizeCard = getSizeCard(typeSizeCard)

  // Проверяем, есть ли свойства minHeight и maxWidth
  if ('minHeight' in sizeCard && 'maxWidth' in sizeCard) {
    const numericMinHeight = extractNumber(sizeCard.minHeight)
    const numericMaxWidth = extractNumber(sizeCard.maxWidth)

    return (
      <ContentLoader
        speed={2}
        width={numericMaxWidth}
        height={numericMinHeight}
        backgroundColor="var(--skeleton-background)"
        foregroundColor="var(--skeleton-foreground-color)"
        style={{ borderRadius: '20px' }}
      >
        {/* Прямоугольники для изображения */}
        <rect x="0" y="0" rx="20" ry="20" width={numericMaxWidth} height={numericMinHeight * 0.6} />
        {/* Прямоугольники для текста */}

        <rect x="10" y={numericMinHeight * 0.65} rx="5" ry="5" width="70%" height="15" />
        <rect x="10" y={numericMinHeight * 0.75} rx="5" ry="5" width="50%" height="15" />
        <rect x="10" y={numericMinHeight * 0.85} rx="5" ry="5" width="30%" height="15" />
      </ContentLoader>
    )
  }

  // Если у нас flexGrow, используем другие стили
  return (
    <ContentLoader
      speed={2}
      width="100%"
      height="200px"
      backgroundColor="var(--skeleton-background)"
      foregroundColor="var(--skeleton-foreground-color)"
      style={{ borderRadius: '20px', flex: sizeCard.flex }}
    >
      <rect x="0" y="0" rx="20" ry="20" width="100%" height="60%" />
      <rect x="10" y="65%" rx="5" ry="5" width="70%" height="15" />
      <rect x="10" y="75%" rx="5" ry="5" width="50%" height="15" />
      <rect x="10" y="85%" rx="5" ry="5" width="30%" height="15" />
    </ContentLoader>
  )
}

export default StandardCardSkeleton
