import { FC } from 'react'

import { HotDealLabel } from 'shared/ui/labels/HotDealLabel'

import { getSizeCard } from '../utils/getSizeCard'

export interface IStandardCardProps {
  typeSizeCard: 'small' | 'middle' | 'flexGrow'
  backgroundImage?: string
  favoriteButton?: React.ReactNode
  ratingBlock?: React.ReactNode
  tripName?: string
  country?: string
  price?: number
  isHotOffer?: boolean
}

const StandardCard: FC<IStandardCardProps> = ({
  typeSizeCard,
  backgroundImage,
  favoriteButton,
  ratingBlock,
  tripName,
  country,
  price,
  isHotOffer,
}) => {
  return (
    <div
      style={{
        ...getSizeCard(typeSizeCard),
        background: backgroundImage ? `url(${backgroundImage}) center center` : 'grey',
        borderRadius: '20px',
        position: 'relative',
      }}
    >
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          alignItems: 'center',
          margin: '0.625rem',
          gap: '0.25rem',
        }}
      >
        {isHotOffer && <HotDealLabel />}
        {favoriteButton}
        {ratingBlock}
      </div>
      <div style={{ position: 'absolute', bottom: '1rem', left: '1rem' }}>
        <h2
          style={{
            fontFamily: 'Poppins,sans-serif',
            fontWeight: 600,
            lineHeight: '143%',
            color: 'var(--primary-white)',
            fontSize: '14px',
          }}
        >
          {tripName && <>{tripName},</>}
        </h2>
        <h2
          style={{
            fontFamily: 'Poppins,sans-serif',
            fontWeight: 600,
            lineHeight: '143%',
            color: 'var(--primary-white)',
            fontSize: '14px',
          }}
        >
          {country}
        </h2>
        <h3
          style={{
            fontFamily: 'Poppins,sans-serif',
            fontWeight: 500,
            lineHeight: '160%',
            color: 'var(--primary-white)',
            fontSize: '10px',
          }}
        >
          {price && <>Starting at $ {price}</>}
        </h3>
      </div>
    </div>
  )
}

export default StandardCard
