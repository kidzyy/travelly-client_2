import { IStandardCardProps } from '../ui'

type TGetSizeCard = IStandardCardProps
type TGetSizeCardReturnDefault = {
  maxWidth: string
  minHeight: string
}
type TGetSizeCardReturnFlexible = {
  flex: string
}
type TGetSizeCardUnion = TGetSizeCardReturnDefault | TGetSizeCardReturnFlexible

export const getSizeCard = (typeSizeCard: TGetSizeCard['typeSizeCard']): TGetSizeCardUnion => {
  switch (typeSizeCard) {
    case 'small':
      return {
        maxWidth: '155px',
        minHeight: '210px',
      }
    case 'middle':
      return {
        maxWidth: '343px',
        minHeight: '250px',
      }
    case 'flexGrow':
      return {
        flex: '1 1 46.3%',
        minHeight: '210px',
      }
    default:
      const exhaustiveCheck: never = typeSizeCard
      throw new Error(exhaustiveCheck)
  }
}
