import { FC } from 'react'

import { SvgIcon } from 'shared/ui/svg/svgIcon/SvgIcon'
import { ISvgIconCustom } from 'shared/ui/svg/types'

const PhoneIcon: FC<ISvgIconCustom> = ({ color, width, height }) => {
  return (
    <SvgIcon color={color} width={width} height={height}>
      <path
        d="M17.6241 24.2091C19.9926 25.4012 22.7968 26.2484 26.1035 26.5794C26.6783 26.6369 27.1666 26.1777 27.1666 25.6001V22.1141C27.1666 21.6552 26.8543 21.2553 26.4091 21.144L22.3825 20.1373C22.0418 20.0521 21.6813 20.152 21.4329 20.4004L17.6241 24.2091ZM17.6241 24.2091C13.4699 22.1183 10.656 18.9666 8.82147 15.6784M8.82147 15.6784C6.99528 12.4052 6.13963 8.99658 5.89852 6.3646C5.84694 5.80161 6.3018 5.33333 6.86714 5.33333H10.3468C10.8235 5.33333 11.2339 5.66979 11.3274 6.13721L12.395 11.4752C12.4605 11.8031 12.3579 12.142 12.1215 12.3784L8.82147 15.6784Z"
        stroke={color}
        strokeWidth="2"
      />
    </SvgIcon>
  )
}

export default PhoneIcon
