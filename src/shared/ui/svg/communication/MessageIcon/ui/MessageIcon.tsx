import { FC } from 'react'

import { SvgIcon } from 'shared/ui/svg/svgIcon/SvgIcon'
import { ISvgIconCustom } from 'shared/ui/svg/types'

const MessageIcon: FC<ISvgIconCustom> = ({ color, width, height }) => {
  return (
    <SvgIcon color={color} width={width} height={height}>
      <path
        d="M5.83325 9.33333L14.6999 15.9833C15.7666 16.7833 17.2333 16.7833 18.2999 15.9833L27.1666 9.33327"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <rect
        x="4.5"
        y="6.66667"
        width="24"
        height="18.6667"
        rx="2"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
      />
    </SvgIcon>
  )
}

export default MessageIcon
