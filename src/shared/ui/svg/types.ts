import { ReactNode } from 'react'

export interface ISvgIcon {
  color: string
  width: number
  height: number
  children: ReactNode
}

export interface ISvgIconCustom extends Omit<ISvgIcon, 'children'> {}
