import { FC } from 'react'

import { SvgIcon } from 'shared/ui/svg/svgIcon/SvgIcon'
import { ISvgIconCustom } from 'shared/ui/svg/types'

const GoogleIcon: FC<ISvgIconCustom> = ({ color, width, height }) => {
  return (
    <SvgIcon color={color} width={width} height={height}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M30.5 15.341C30.5 14.2773 30.4026 13.2546 30.2218 12.2728H15.8062V18.0751H24.0436C23.6888 19.9501 22.6104 21.5387 20.9894 22.6024V26.366H25.936C28.8303 23.7546 30.5 19.9092 30.5 15.341Z"
        fill={color}
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M15.8056 30.0001C19.9382 30.0001 23.403 28.657 25.9354 26.366L20.9888 22.6024C19.6182 23.5024 17.8649 24.0342 15.8056 24.0342C11.819 24.0342 8.4447 21.3956 7.24108 17.8501H2.12744V21.7365C4.646 26.6388 9.82226 30.0001 15.8056 30.0001Z"
        fill={color}
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.24166 17.8501C6.93553 16.9501 6.7616 15.9887 6.7616 15.0001C6.7616 14.0114 6.93553 13.0501 7.24166 12.1501V8.26367H2.12802C1.09137 10.2887 0.5 12.5796 0.5 15.0001C0.5 17.4205 1.09137 19.7114 2.12802 21.7365L7.24166 17.8501Z"
        fill={color}
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M15.8056 5.96593C18.0528 5.96593 20.0704 6.72275 21.6567 8.20912L26.0468 3.90683C23.396 1.48637 19.9313 0 15.8056 0C9.82226 0 4.646 3.36138 2.12744 8.26367L7.24108 12.15C8.4447 8.60458 11.819 5.96593 15.8056 5.96593Z"
        fill={color}
      />
    </SvgIcon>
  )
}

export default GoogleIcon
