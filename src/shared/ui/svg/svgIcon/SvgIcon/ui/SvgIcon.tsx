import { FC } from 'react'

import { ISvgIcon } from '../../../types'

const SvgIcon: FC<ISvgIcon> = ({ color, width, height, children }) => {
  return (
    <svg width={width} height={height} viewBox={`0 0 ${width} ${height}`} fill="none">
      {children}
    </svg>
  )
}

export default SvgIcon
