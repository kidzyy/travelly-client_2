import { FC } from 'react'

import { Text } from 'shared/ui/text/Text'

import style from './index.module.css'

interface IStartPageLogo {
  title?: string
  color: string
}

const StartPageLogo: FC<IStartPageLogo> = ({ title, color }) => (
  <div className={style['startPage-logo']}>
    <svg width="64" height="48" viewBox="0 0 64 48" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M37.3209 0.00597398V9.37233H24.537V48H12.784V9.37233H0V0.00597398H37.3209Z" fill={color} />
      <path d="M26.6791 47.994V38.6277H39.463V0H51.216V38.6277H64V47.994H26.6791Z" fill={color} />
    </svg>
    <Text tag="h2" style={{ color: `${color}` }} className={style['startPage-logo-title']}>
      {title}
    </Text>
  </div>
)

export default StartPageLogo
