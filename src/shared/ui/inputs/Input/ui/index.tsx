import React from 'react';

interface IInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  className?: string;
}

const Input: React.FC<IInputProps> = ({ className, ...props }) => {
  return <input className={className} {...props} />;
}

export default Input;
