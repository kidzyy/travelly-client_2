import { FC } from 'react'

import style from './index.module.css'

interface ITrackBar {
  currentStep: number
  quantityTrackItem?: number
}

const TrackBar: FC<ITrackBar> = ({ currentStep, quantityTrackItem = 3 }) => {
  return (
    <div className={style['start-step-track']}>
      <div className={style['start-step-track']}>
        {Array.from({ length: quantityTrackItem }, (_, index) => (
          <span key={index} className={currentStep === index ? style['black-span'] : style['grey-span']}></span>
        ))}
      </div>
    </div>
  )
}
export default TrackBar
