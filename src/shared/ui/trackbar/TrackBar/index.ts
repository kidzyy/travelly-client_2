import { lazy } from 'react'

const TrackBar = lazy(() => import('shared/ui/trackbar/TrackBar/ui/TrackBar'))
export { TrackBar }
