import { PayloadAction, createSlice } from '@reduxjs/toolkit'

import { RootState } from 'app/store'

interface ITrackBarState {
  currentStep: number
}

const initialTrackBarState: ITrackBarState = {
  currentStep: 0,
}

const trackBarStepSlice = createSlice({
  name: 'trackBarStep',
  initialState: initialTrackBarState,
  reducers: {
    setCurrentStep: (state, { payload }: PayloadAction<number>) => {
      state.currentStep = payload
    },
  },
})

export const { setCurrentStep } = trackBarStepSlice.actions
export default trackBarStepSlice.reducer

export const selectCurrentStep = (state: RootState) => state.trackBarStep.currentStep
