import { FC } from 'react'

import { CSSPropertiesStyled } from 'shared/types/css'

import style from './index.module.css'

const MarkerLight: FC<CSSPropertiesStyled> = ({ styles }) => {
  return (
    <div style={styles} className={style['marker-dot']}>
      <div className={style['innerMarkerLight-dot']}></div>
      <div className={style['outerMarkerLight-dot']}></div>
    </div>
  )
}

export default MarkerLight
