import { FC } from 'react'

interface ICustomImageProps {
  className: string
  alt: string
  src: string
}

const CustomImage: FC<ICustomImageProps> = ({ className, alt, src }) => {
  return <img className={className} src={require(`shared/assets/images/${src}`)} alt={alt} />
}

export default CustomImage
