const HotDealLabel = () => {
  return (
    <div
      style={{
        width: '1.25rem',
        height: '1.25rem',
        backgroundColor: 'var(--primary-white)',
        borderRadius: '7px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
          d="M8.40937 6.9375C8.32859 7.448 8.08824 7.9198 7.72277 8.28527C7.3573 8.65074 6.8855 8.89109 6.375 8.97187"
          stroke="#FF6E1C"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M3.375 3.06567C2.65313 4.15317 2.0625 5.40005 2.0625 6.56255C2.0625 7.60684 2.47734 8.60836 3.21577 9.34678C3.95419 10.0852 4.95571 10.5 6 10.5C7.04429 10.5 8.04581 10.0852 8.78423 9.34678C9.52266 8.60836 9.9375 7.60684 9.9375 6.56255C9.9375 4.12505 8.25 2.25005 6.88125 0.885986L5.25 4.31255L3.375 3.06567Z"
          stroke="#FF6E1C"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    </div>
  )
}

export default HotDealLabel
