import React, { CSSProperties, FC, ReactNode } from 'react'

interface TextProps {
  tag:
    | 'h1'
    | 'h2'
    | 'h3'
    | 'h4'
    | 'h5'
    | 'h6'
    | 'p'
    | 'span'
    | 'strong'
    | 'em'
    | 'sub'
    | 'sup'
    | 'blockquote'
    | 'code'
    | 'pre'
  className?: string
  style?: CSSProperties
  children?: ReactNode
}

const Text: FC<TextProps> = ({ tag, className, style, children }) => {
  return React.createElement(tag, { className, style }, children)
}

export default Text
