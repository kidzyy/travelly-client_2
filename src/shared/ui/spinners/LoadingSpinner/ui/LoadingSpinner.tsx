import { FC } from 'react'

import { CgSpinner } from 'react-icons/cg'

import style from './index.module.css'

interface ILoadingSpinner {
  size: number
}

const LoadingSpinner: FC<ILoadingSpinner> = ({ size }) => <CgSpinner size={size} className={style['loading-spinner']} />

export default LoadingSpinner
