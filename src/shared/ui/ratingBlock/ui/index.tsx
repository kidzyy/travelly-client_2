interface IRatingBlockProps {
  variation: 'extra_sm_card'
  styles: React.CSSProperties
  rating: number
}

type IRatingBlockStyles = Record<string, { width: string; height: string }>

const ratingBlockStyles: IRatingBlockStyles = {
  extra_sm_card: {
    width: '40px',
    height: '20px',
  },
}

const RatingBlock = ({ variation, styles, rating }: IRatingBlockProps) => {
  const sizeStyle = ratingBlockStyles[variation]
  return (
    <div style={{ ...sizeStyle, ...styles }}>
      <svg
        style={{ display: 'flex', flex: '1' }}
        width="16"
        height="16"
        viewBox="0 0 16 16"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M7.04895 3.5938C7.3483 2.67249 8.65171 2.67249 8.95106 3.5938L9.42193 5.04297C9.5558 5.455 9.93976 5.73396 10.373 5.73396H11.8967C12.8655 5.73396 13.2682 6.97357 12.4845 7.54297L11.2518 8.43861C10.9013 8.69326 10.7546 9.14462 10.8885 9.55665L11.3594 11.0058C11.6587 11.9271 10.6042 12.6933 9.82053 12.1239L8.58779 11.2282C8.2373 10.9736 7.76271 10.9736 7.41222 11.2282L6.17948 12.1239C5.39577 12.6933 4.34128 11.9271 4.64064 11.0058L5.1115 9.55665C5.24538 9.14462 5.09872 8.69326 4.74823 8.43861L3.51549 7.54297C2.73178 6.97357 3.13455 5.73396 4.10327 5.73396H5.62703C6.06025 5.73396 6.44421 5.455 6.57808 5.04297L7.04895 3.5938Z"
          fill="var(--primary-orange)"
        />
      </svg>
      <div
        style={{
          fontFamily: 'Poppins',
          fontWeight: 500,
          fontSize: '10px',
          lineHeight: '160%',
          color: 'var(--primary-orange)',
          display: 'flex',
          flex: '1',
        }}
      >
        {rating}
      </div>
    </div>
  )
}

export default RatingBlock
