import { CSSProperties, FC } from 'react'

import { greyFontsOptions } from 'shared/fonts/styleFonts'

import style from './index.module.css'

interface ILineTitleCenter {
  title: string
  styles?: CSSProperties
}

const LineTitleCenter: FC<ILineTitleCenter> = ({ title, styles }) => (
  <span style={{ ...styles, ...greyFontsOptions.medium.medium_grey }} className={style['line-with-text']}>
    {title}
  </span>
)

export default LineTitleCenter
