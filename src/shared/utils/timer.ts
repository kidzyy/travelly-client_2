export const timer = (delay: number) => new Promise(res => setTimeout(res, delay))
