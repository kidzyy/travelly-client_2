import { ReactNode } from 'react'

interface PageTransitionProps {
  children: ReactNode
  classNames?: string
  timeout?: number | { enter?: number; exit?: number }
  in: boolean
  unmountOnExit?: any
}

export default PageTransitionProps
