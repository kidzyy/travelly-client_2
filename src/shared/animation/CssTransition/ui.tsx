import { memo } from 'react'

import { CSSTransition } from 'react-transition-group'
import PageTransitionProps from './model'

const PageTransition: React.FC<PageTransitionProps> = memo(({ children, ...props }) => (
  <CSSTransition {...props}>{children}</CSSTransition>
))

export default PageTransition
