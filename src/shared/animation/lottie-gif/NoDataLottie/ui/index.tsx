import { FC } from 'react'

import Lottie from 'react-lottie'

import animationData from './Lottie_NoData.json'

interface INoDataLottie {
  title?: string
}

const NoDataLottie: FC<INoDataLottie> = ({ title }) => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice', // Настройки рендеринга
    },
  }

  return (
    <div style={{ textAlign: 'center', marginTop: '20px', position: 'relative' }}>
      <Lottie options={defaultOptions} height={400} width={400} />
      <p
        style={{
          fontFamily: 'Poppins,sans-serif',
          fontWeight: 600,
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%,-50%)',
        }}
      >
        {title}
      </p>
    </div>
  )
}

export default NoDataLottie
