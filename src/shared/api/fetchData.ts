import { UseQueryResult, useQuery } from '@tanstack/react-query'
import axios, { AxiosResponse } from 'axios'

async function fetchDataGet<T>(path: string): Promise<T> {
  try {
    const response: AxiosResponse<T> = await axios.get(`http://localhost:3001/${path}`)
    return response.data
  } catch (error) {
    throw error
  }
}

export function useGetData<T>(key: string): UseQueryResult<T, unknown> {
  return useQuery<T, unknown>({
    queryKey: [key], // ключ запроса должен быть массивом
    queryFn: () => fetchDataGet<T>(key),
    refetchOnWindowFocus: false,
    placeholderData: undefined,
  })
}

interface IFetcher {
  get<T>(url: string): Promise<T>
  post<T>(url: string, body: any): Promise<T>
  put<T>(url: string, body: any): Promise<T>
  patch<T>(url: string, body: any): Promise<T>
  delete<T>(url: string): Promise<T>
}

export class Fetcher implements IFetcher {
  constructor(private baseUrl: string) {}

  async get<T>(url: string): Promise<T> {
    const response = await axios.get(`${this.baseUrl}${url}`)
    return response.data
  }

  async post<T>(url: string, body: any): Promise<T> {
    try {
      const response = await axios.post(`${this.baseUrl}${url}`, body)
      return response.data
    } catch (error) {
      console.error('Error during POST request:', error)
      throw error
    }
  }

  async put<T>(url: string, body: any): Promise<T> {
    const response = await axios.put(`${this.baseUrl}${url}`, body)
    return response.data
  }

  async patch<T>(url: string, body: any): Promise<T> {
    const response = await axios.patch(`${this.baseUrl}${url}`, body)
    return response.data
  }

  async delete<T>(url: string): Promise<T> {
    const response = await axios.delete(`${this.baseUrl}${url}`)
    return response.data
  }
}
