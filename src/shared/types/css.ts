import { CSSProperties } from 'react'

export interface CSSPropertiesStyled {
  styles: CSSProperties
}
