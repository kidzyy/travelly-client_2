import { IWelcomeDataWithImage } from 'entities/welcome/model/types'

export type TStartPageCurrentData = IWelcomeDataWithImage | null
