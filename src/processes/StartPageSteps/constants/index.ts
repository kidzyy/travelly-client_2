export const stylesStartPageSteps: React.CSSProperties = {
  backgroundColor: 'var(--primary-orange)',
  color: 'var(--primary-white)',
  position: 'absolute',
  bottom: 0,
  marginBottom: '3rem',
} as const

export const LAST_STEP: number = 3
