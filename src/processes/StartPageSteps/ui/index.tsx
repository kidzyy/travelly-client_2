import { FC, useEffect } from 'react'

import { useNavigate } from 'react-router-dom'
import { TransitionGroup } from 'react-transition-group'

import { PageTransition } from 'shared/animation/CssTransition'
import { useGetData } from 'shared/api/fetchData'
import { blackFontsOptions } from 'shared/fonts/styleFonts'
import { useNextStepHandler } from 'shared/hooks/nextStepHandler'
import { Layout } from 'shared/layouts/Layout'
import 'shared/routes/ui/transition.css'
import { MainButton } from 'shared/ui/buttons/MainButton'
import { CustomImage } from 'shared/ui/image/CustomImage'
import { Text } from 'shared/ui/text/Text'
import { TrackBar } from 'shared/ui/trackbar/TrackBar'

import { IWelcomeDataWithImage } from 'entities/welcome/model/types'

import style from './index.module.css'

import { LAST_STEP, stylesStartPageSteps } from '../constants'
import { TStartPageCurrentData } from '../types'

const StartPageSteps: FC = () => {
  const { isLoading, isError, data, isSuccess } = useGetData<IWelcomeDataWithImage[]>('welcome')
  const { currentStep, handleNextStepClick, finalStep } = useNextStepHandler<IWelcomeDataWithImage>()
  const navigate = useNavigate()

  const startPageCurrentData: TStartPageCurrentData = data ? data[currentStep] : null

  useEffect(() => {
    finalStep === LAST_STEP && navigate('/welcomeMain')
  }, [currentStep])

  return (
    <Layout>
      {isLoading && <div>Загрузка...</div>}
      {isError && <div>Возникла ошибка сервера...</div>}
      {isSuccess && (
        <>
          <TrackBar quantityTrackItem={3} currentStep={currentStep} />
          <TransitionGroup>
            <PageTransition
              key={startPageCurrentData ? startPageCurrentData.id : null}
              in={true}
              timeout={300}
              classNames="fade-in"
            >
              {startPageCurrentData && (
                <>
                  <CustomImage
                    className={style['start-step-img']}
                    src={startPageCurrentData.img}
                    alt={startPageCurrentData.title}
                  />

                  <Text tag="h1" style={blackFontsOptions.medium.mediumLg_black} className={style['start-step-title']}>
                    {startPageCurrentData.title}
                  </Text>
                </>
              )}
            </PageTransition>
          </TransitionGroup>

          <MainButton variation={'large'} styles={stylesStartPageSteps} onClick={() => handleNextStepClick(data)}>
            {currentStep === 2 ? 'Let’s start!' : 'Next'}
          </MainButton>
        </>
      )}
    </Layout>
  )
}

export default StartPageSteps
