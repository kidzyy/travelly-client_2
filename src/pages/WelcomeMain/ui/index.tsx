import { FC } from 'react'

import { useNavigate } from 'react-router-dom'

import { Layout } from 'shared/layouts/Layout'
import { MainButton } from 'shared/ui/buttons/MainButton'
import { StartPageLogo } from 'shared/ui/logo/StartPageLogo'
import { Text } from 'shared/ui/text/Text'

import style from './index.module.css'

import { buttonsAuth } from '../constants'

const WelcomeMain: FC = () => {
  const navigate = useNavigate()
  return (
    <section style={{ backgroundColor: 'var(--secondary-white)' }}>
      <Layout>
        <Text
          tag="h2"
          style={{ position: 'relative', color: 'var(--primary-green)', top: '13.75rem' }}
          className={style['welcome-main-title']}
        >
          Welcome to
        </Text>
        <StartPageLogo color={'var(--primary-green)'} title="Travelly" />
        <div style={{ position: 'relative', display: 'flex', flexDirection: 'column', gap: '1rem', top: '55%' }}>
          <MainButton
            variation="large"
            styles={buttonsAuth.buttonSignUp('var(--primary-orange)', 'var(--primary-white)')}
            onClick={() => navigate('/sign-up')}
          >
            Sign Up
          </MainButton>
          <MainButton
            variation="large"
            styles={buttonsAuth.buttonLogin('var(--primary-white)', 'var(--primary-orange)')}
            onClick={() => navigate('/log-in')}
          >
            Login
          </MainButton>
        </div>
      </Layout>
    </section>
  )
}

export default WelcomeMain
