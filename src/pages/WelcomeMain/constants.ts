import { CSSPropertiesStyled } from 'shared/types/css'

interface IButtonsAuthStyled<T> {
  buttonLogin: (backgroundColor: string, color: string) => {}
  buttonSignUp: (backgroundColor: string, color: string) => {}
}

export const buttonsAuth: IButtonsAuthStyled<CSSPropertiesStyled> = {
  buttonLogin(backgroundColor, color) {
    return {
      backgroundColor: backgroundColor,
      color: color,
    }
  },
  buttonSignUp(backgroundColor, color) {
    return {
      backgroundColor: backgroundColor,
      color: color,
    }
  },
}
