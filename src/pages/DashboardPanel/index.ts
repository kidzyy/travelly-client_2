import { TDashboardPanelPages } from 'features/tabs/RouteTabs/ui'

import DashboardPanel from './ui'

export { DashboardPanel }

export type { TDashboardPanelPages }
