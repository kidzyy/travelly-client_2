import { useEffect } from 'react'

import moment from 'moment'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import { logout } from 'entities/users/authSlice'

import { RouteTabs, TDashboardPanelPages, routeElements } from 'features/tabs/RouteTabs'

import { BookingTicketsPanel } from '../booking'
import { HomePage } from '../home'

export const NotificationPage = () => {
  return <div>Notification Content</div>
}

export const AccountPage = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  return (
    <button
      style={{ width: '20px', height: '20px' }}
      onClick={() => {
        dispatch(logout())
        navigate('/welcomeMain', { replace: true })
      }}
    ></button>
  )
}

const dashboardPages: TDashboardPanelPages = [
  <HomePage />,
  <BookingTicketsPanel />,
  <NotificationPage />,
  <AccountPage />,
]

const DashboardPanel = () => {
  useEffect(() => {
    console.log(moment('2024-10-10').format('ll'))
  }, [])
  return <RouteTabs tabs={routeElements} pages={dashboardPages} />
}

export default DashboardPanel
