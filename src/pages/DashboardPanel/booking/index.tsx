import { FC } from 'react'

import { useNavigate } from 'react-router-dom'

import style from './style.module.css'

import eventsImage from '../../../assets/booking/booking_events_ticket_image.png'
import hotelImage from '../../../assets/booking/booking_hotel_ticket_image.png'
import transportImage from '../../../assets/booking/booking_transport_ticket_image.png'
import tripsImage from '../../../assets/booking/booking_trips_ticket_image.png'

interface IBookingTicketPanel {
  image: string
  title: string
}

const BookingTicketPanel: FC<IBookingTicketPanel> = ({ image, title }) => {
  const navigate = useNavigate()
  return (
    <picture
      onClick={() => navigate(`/${title.toLowerCase()}_searchableForm`)}
      className={style['booking-select-ticket']}
    >
      <svg width="343" height="311" viewBox="0 0 343 311" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M0 20C0 8.95431 8.9543 0 20 0H323C334.046 0 343 8.9543 343 20V249C336.373 249 331 254.373 331 261C331 267.627 336.373 273 343 273V291C343 302.046 334.046 311 323 311H20C8.9543 311 0 302.046 0 291V273C6.62742 273 12 267.627 12 261C12 254.373 6.62742 249 0 249V20Z"
          fill="white"
        />
      </svg>
      <h2 className={style['booking-select-ticket-items__title']}>{title}</h2>
      <img
        style={{ position: 'absolute', width: '100%', maxWidth: '327px', transform: 'translateY(-20px)' }}
        src={image}
        alt="Some logo"
      />
    </picture>
  )
}

export const BookingTicketsPanel = () => {
  const bookingTicketElements = [
    {
      title: 'Hotel',
      image: hotelImage,
    },
    {
      title: 'Trips',
      image: tripsImage,
    },
    {
      title: 'Transport',
      image: transportImage,
    },
    {
      title: 'Events',
      image: eventsImage,
    },
  ]
  return (
    <section className={style['booking-select-ticket-items']}>
      <h1 className={style['booking-select-ticket-items__main-title']}>Booking</h1>
      {bookingTicketElements.map(({ title, image }, index) => (
        <BookingTicketPanel key={index} title={title} image={image} />
      ))}
    </section>
  )
}
