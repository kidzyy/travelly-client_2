import { Layout } from 'shared/layouts/Layout'
import { Text } from 'shared/ui/text/Text'

import { useFilterTripData } from 'entities/cards/TripCard/hooks/useFilterTripData'

import FilterListSelector from 'features/filters/FilterListSelector/ui'
import { SearchInput } from 'features/inputs/SearchInput'

import { TripCardsList } from 'widgets/cards/TripCardsList'

import style from './style.module.css'

export const HomePage = () => {
  const { selectedFilter, setSelectedFilter, filterTripDataOptions, applyFilter } = useFilterTripData()

  return (
    <section className={style['home-page__section']}>
      <Layout style={{ height: '80vh' }}>
        <div className={style['home-page-description']}>
          <Text className={style['home-page__description-title']} tag="h2">
            Discover
          </Text>
          <Text className={style['home-page__description-subtitle']} tag="h3">
            Explore the best places in the world!
          </Text>
        </div>
        <div style={{ display: 'flex', flexDirection: 'column', gap: '14px' }}>
          <SearchInput />
          <FilterListSelector
            filterOptions={filterTripDataOptions}
            onFilterChange={setSelectedFilter}
            selectedFilter={selectedFilter}
          />
          <TripCardsList searchEnabled applyFilter={cards => applyFilter(selectedFilter, cards)} />
        </div>
      </Layout>
    </section>
  )
}
