import { blackFontsOptions } from 'shared/fonts/styleFonts'
import { Layout } from 'shared/layouts/Layout'
import { SmallTouchButton } from 'shared/ui/buttons/SmallTouchButton'
import { LineTitleCenter } from 'shared/ui/lines/LineTitleCenter'
import { AppleIcon } from 'shared/ui/svg/social-network/AppleIcon'
import { FacebookIcon } from 'shared/ui/svg/social-network/FacebookIcon'
import { GoogleIcon } from 'shared/ui/svg/social-network/GoogleIcon'

import { BackButton } from 'features/buttons/BackButton'
import { SignInForm } from 'features/forms/sign-in-form'

const SignInPage = () => {
  return (
    <section style={{ padding: '20px 0 20px 0' }}>
      <Layout>
        <BackButton type="page" />
        <h2 style={{ margin: '7rem 0 0 0', textAlign: 'center', ...blackFontsOptions.semiBold.largeMd_black }}>
          Great to meet You!
        </h2>
        <h3 style={{ textAlign: 'center', ...blackFontsOptions.semiBold.medium_mLgLineH_black }}>
          Sign up and let’s get started
        </h3>
        <SignInForm />
        <LineTitleCenter title="or" styles={{ marginTop: '2rem' }} />
        <div
          style={{
            gap: '1rem',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: '2rem',
          }}
        >
          <SmallTouchButton variation="sm" styles={{ backgroundColor: 'var(--primary-white)' }}>
            <FacebookIcon color={'var(--primary-orange)'} width={17} height={32} />
          </SmallTouchButton>
          <SmallTouchButton variation="sm" styles={{ backgroundColor: 'var(--primary-white)' }}>
            <GoogleIcon color={'var(--primary-orange)'} width={31} height={30} />
          </SmallTouchButton>
          <SmallTouchButton variation="sm" styles={{ backgroundColor: 'var(--primary-white)' }}>
            <AppleIcon color={'var(--primary-orange)'} width={29} height={32} />
          </SmallTouchButton>
        </div>
      </Layout>
    </section>
  )
}

export default SignInPage
