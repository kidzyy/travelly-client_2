import { blackFontsOptions } from 'shared/fonts/styleFonts'
import { Layout } from 'shared/layouts/Layout'

import { BackButton } from 'features/buttons/BackButton'
import SignUpForm from 'features/forms/sign-up-form/ui'

const SignUpPage = () => {
  return (
    <section style={{ padding: '20px 0 20px 0' }}>
      <Layout>
        <BackButton type="page" />
        <h2 style={{ margin: '3rem 0 0 0', textAlign: 'center', ...blackFontsOptions.semiBold.largeMd_black }}>
          Welcome back!
        </h2>
        <h3 style={{ textAlign: 'center', ...blackFontsOptions.semiBold.medium_mLgLineH_black }}>
          Sign in and let’s get going
        </h3>
        <SignUpForm />
      </Layout>
    </section>
  )
}

export default SignUpPage
