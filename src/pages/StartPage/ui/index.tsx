import { FC } from 'react'

import { Preloader } from 'features/Preloader'
import { useNavigate } from 'react-router-dom'
import { Layout } from 'shared/layouts/Layout'
import { StartPageLogo } from 'shared/ui/logo/StartPageLogo'

import style from './index.module.css'

const StartPage: FC = () => {
  const navigate = useNavigate()
  const handleProgressComplete = () => navigate('/welcome')

  return (
    <section className={style['start-page-section']}>
      <Layout>
        <StartPageLogo color={'var(--primary-white)'} title={'Travelly'} />
        <Preloader onProgressComplete={handleProgressComplete} />
      </Layout>
    </section>
  )
}

export default StartPage
