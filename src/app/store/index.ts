import { configureStore } from '@reduxjs/toolkit'

import authReducer from '../../entities/users/authSlice'
import searchInputSlice from '../../features/inputs/SearchInput/model/slice'
import trackBarStepReducer from '../../shared/ui/trackbar/TrackBar/api/trackBarSlice'

export const store = configureStore({
  reducer: {
    trackBarStep: trackBarStepReducer,
    searchInputSlice: searchInputSlice,
    auth: authReducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: ['your/actionType'],
        ignoredPaths: ['signUpStepsReducer.user'],
      },
    }),
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
