export interface RouteConfig<T> {
  path: string
  Component: T
}
