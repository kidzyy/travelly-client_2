import { Navigate, Outlet } from 'react-router-dom'

import { useAuth } from '../model/useAuth'

const ProtectedRoute = () => {
  const isAuthenticated = useAuth()

  if (isAuthenticated === null) {
    // Можно отобразить загрузку, пока проверяется аутентификация
    return <div>Loading...</div>
  }

  // Если пользователь не авторизован, перенаправляем на страницу логина
  if (!isAuthenticated) {
    return <Navigate to="/welcomeMain" replace />
  }

  // Если авторизован, отображаем нужный маршрут
  return <Outlet />
}

export default ProtectedRoute
