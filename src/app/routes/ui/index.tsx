import { Location, Route, Routes } from 'react-router-dom'

import { PageTransition } from 'shared/animation/CssTransition'

import { routesAuthPage } from 'app/routes/constants/routeAuthPage'
import { routesStartPage } from 'app/routes/constants/routeStartPage'

import { routesDashboardPanel } from '../constants/routeDashboardPanel'
import { routesSearchableBookingForm } from '../constants/routeSearchableBooking'
import ProtectedRoute from './ProtectedRoute'

const RouteScreens = ({ location }: { location: Location }) => {
  const animationConfig = {
    classNames: 'fade-in',
    timeout: 300,
  }

  return (
    <>
      <PageTransition key={location.pathname} in={true} {...animationConfig}>
        <Routes location={location}>
          {routesStartPage.map(({ path, Component }) => (
            <Route key={path} path={path} element={<Component />} />
          ))}
          {routesAuthPage.map(({ path, Component }) => (
            <Route key={path} path={path} element={<Component />} />
          ))}
          {/* Защищённые маршруты */}
          <Route element={<ProtectedRoute />}>
            {routesDashboardPanel.map(({ path, Component }) => (
              <Route key={path} path={path} element={<Component />} />
            ))}
            {routesSearchableBookingForm.map(({ path, Component }) => (
              <Route key={path} path={path} element={<Component />} />
            ))}
          </Route>
        </Routes>
      </PageTransition>
    </>
  )
}

export default RouteScreens
