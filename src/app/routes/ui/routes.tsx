import { Suspense, lazy } from 'react'

import { useLocation } from 'react-router-dom'
import { TransitionGroup } from 'react-transition-group'

import 'shared/routes/ui/transition.css'

const RouteScreens = lazy(() => import('.'))

const AppRoutes = () => {
  const location = useLocation()

  return (
    <Suspense>
      <TransitionGroup component={null}>
        <RouteScreens location={location} />
      </TransitionGroup>
    </Suspense>
  )
}

export default AppRoutes
