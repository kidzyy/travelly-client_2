import { lazy } from 'react'

import { RouteConfig } from 'app/routes/model/types'


const SearchableBookingForm = lazy(() => import('widgets/searchables/SearchableBookingForm/ui'))

export const routesSearchableBookingForm: RouteConfig<React.ComponentType>[] = [
  { path: '/hotel_searchableForm', Component: SearchableBookingForm },
  { path: '/trips_searchableForm', Component: SearchableBookingForm },
  { path: '/transport_searchableForm', Component: SearchableBookingForm },
  { path: '/events_searchableForm', Component: SearchableBookingForm },
]
