import { lazy } from 'react'

import { RouteConfig } from 'app/routes/model/types'

const DashboardPanel = lazy(() => import('pages/DashboardPanel/ui'))

export const routesDashboardPanel: RouteConfig<React.ComponentType>[] = [{ path: '/dashboard', Component: DashboardPanel }]
