import { lazy } from 'react'

import { RouteConfig } from 'app/routes/model/types'

const SignIn = lazy(() => import('pages/SignInPage/ui'))
const SignUpPage = lazy(() => import('pages/SignUpPage/ui'))

export const routesAuthPage: RouteConfig<React.ComponentType>[] = [
  { path: '/sign-up', Component: SignUpPage },
  { path: '/log-in', Component: SignIn },
]
