import { lazy } from 'react'

import { RouteConfig } from 'app/routes/model/types'

const StartPage = lazy(() => import('pages/StartPage/ui'))
const StartPageSteps = lazy(() => import('processes/StartPageSteps/ui'))
const WelcomeMain = lazy(() => import('pages/WelcomeMain/ui'))

export const routesStartPage: RouteConfig<React.ComponentType>[] = [
  { path: '/', Component: StartPage },
  { path: '/welcome', Component: StartPageSteps },
  { path: '/welcomeMain', Component: WelcomeMain },
]
